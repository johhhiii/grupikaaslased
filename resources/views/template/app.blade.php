<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Grupikaaslased</title>
    </head>
    <body>
        <!-- header -->
        <h1><a href="{{ route('index') }}">Grupikaaslased</a></h1>
        @if(!Auth::check())
        <a href="{{ route('auth.login') }}">logi sisse</a> | <a href="{{ route('auth.register') }}">registreeri</a>
        @else
        <a href="{{ route('user.edit') }}">minu profiil</a> |
        <a href="{{ route('user.all') }}">otsi inimesi</a> |
        <a href="{{ route('auth.logout') }}">logi välja</a>
        @endif
        <br>
        <!-- /header -->

        @yield('content')

        <!-- footer -->
        <br />
        <small>ventt &copy; 2017</small>
        <!-- /footer -->

        <!-- js -->
        @yield('script')
        <!-- /js -->
    </body>
</html>
