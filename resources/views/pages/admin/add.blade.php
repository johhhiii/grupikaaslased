@extends('template.app')

@section('content')
<br>
{!! Form::open(['action' => 'AdminController@postAddUser']) !!}

{!! Form::text('username', null, ['placeholder' => 'kasutajanimi']) !!}<br>
{!! Form::password('password', null, ['placeholder' => 'parool']) !!}<br>
{!! Form::text('firstname', null, ['placeholder' => 'eesnimi']) !!}<br>
{!! Form::text('lastname', null, ['placeholder' => 'perekonnanimi']) !!}<br>
{!! Form::text('sex', null, ['placeholder' => 'sugu (1 on poiss ja 2 on tüdruk)']) !!}<br>
{!! Form::text('day', null, ['placeholder' => 'päev']) !!}<br>
{!! Form::text('month', null, ['placeholder' => 'kuu']) !!}<br>
{!! Form::text('year', null, ['placeholder' => 'aasta']) !!}<br>
{!! Form::text('phone', null, ['placeholder' => 'telo nr']) !!}<br>

{!! Form::submit('lisa uus kasutaja') !!}

{!! Form::close() !!}

@stop
