@extends('template.app')

@section('content')
<br>
{!! Form::open(['action' => 'AuthController@postLogin']) !!}

{!! Form::text('username', null, ['placeholder' => 'kasutajanimi']) !!}<br>
{!! Form::password('password', null, ['placeholder' => 'parool']) !!}<br>
{!! Form::submit('logi sisse') !!}

{!! Form::close() !!}

@stop
