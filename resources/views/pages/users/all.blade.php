@extends('template.app')

@section('content')

{!! Form::open(['action' => 'UserController@search']) !!}
toimib aint kasutajanime järgi rsk, põhimõte siiski säilib <br>
{!! Form::text('search', null, ['placeholder' => 'kasutajanimi']) !!}
{!! Form::submit('otsi') !!}

{!! Form::close() !!}

@foreach($users as $user)
<h4>
    <a href="{{ route('user.view', [$user->id, str_slug($user->lastname)]) }}">{{ $user->firstname }} {{ $user->lastname }}</a>
</h4> - <a href="{{ route('admin.delete', [$user->id]) }}">kustuta</a>
<br>
@endforeach

@if($user->admin == 1)
<a href="{{ route('admin.create') }}">lisa uus kasutaja</a>
@endif

@stop
