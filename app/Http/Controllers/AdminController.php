<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App/User;

class AdminController extends Controller
{
    public function getAddUser() {
        return view('pages.admin.add');
    }
    
    public function postAddUser() {
        $user = new User;

        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);

        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->sex = $request->sex;

        $user->day = $request->day;
        $user->month = $request->month;
        $user->year = $request->year;

        $user->phone = $request->phone;
        $user->image = 'https://placehold.it/200x200/';

        $user->save();

        return redirect()->action('UserController@all');
    }

    public function getDelete($id) {
        $user = User::where(['id' => $id])->delete();

        if($user) {
            return redirect()->action('UserController@all');
        } else {
            abort(404);
        }
    }
}
