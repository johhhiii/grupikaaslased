<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;

use Log;
use Auth;
use Session;

use App\User;

class AuthController extends Controller
{
    public function getLogin() {
        return view('pages.auth.login');
    }

    public function postLogin(LoginRequest $request) {
        if(Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
            return redirect()->action('PageController@index');
        } else {
            return redirect()->action('AuthController@getLogin');
        }
    }

    public function getRegister() {
        return view('pages.auth.register');
    }

    public function postRegister(RegisterRequest $request) {
        try {
            $user = new User;

            $user->username = $request->username;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);

            $user->firstname = $request->firstname;
            $user->lastname = $request->lastname;
            $user->sex = $request->sex;

            $user->day = $request->day;
            $user->month = $request->month;
            $user->year = $request->year;

            $user->phone = $request->phone;
            $user->image = 'https://placehold.it/200x200/';

            $user->save();
        } catch(\Exception $e) {
            Log::error('Viga kasutaja loomisel: ' . $e);
        }

        Auth::login($user);
        Session::put('message', 'Sinu kasutaja on edukalt loodud ning sind logiti juba sisse.');

        return redirect()->action('PageController@index');
    }

    public function logout() {
        Auth::logout();
        return redirect()->action('PageController@index');
    }
}
