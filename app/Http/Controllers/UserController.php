<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\User;

class UserController extends Controller
{
    public function viewUser($id, $slug) {
        $data = [];
        $data['user'] = User::where(['id' => $id])->first();

        return view('pages.users.view', $data);
    }

    public function getEditUser() {
        $data = [];
        $data['user'] = Auth::user();

        return view('pages.users.edit', $data);
    }

    public function postEditUser(Request $request) {
        $user = Auth::user();

        $user->day = $request->day;
        $user->month = $request->month;
        $user->year = $request->year;

        $user->phone = $request->phone;

        $user->save();

        return redirect()->action('UserController@getEditUser');
    }

    public function all() {
        $data = [];
        $data['users'] = User::all();

        return view('pages.users.all', $data);
    }

    public function search(Request $request) {
        $data = [];
        $data['users'] = User::like('username', $request->input('search'))->get();

        return view('pages.users.all', $data);
    }
}
