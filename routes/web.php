<?php

Route::get('/', ['uses' => 'PageController@index', 'as' => 'index']);
Route::get('logout', ['uses' => 'AuthController@logout', 'as' => 'auth.logout']);

Route::group(['prefix' => 'auth', 'middleware' => 'guest'], function () {
    Route::get('login', ['uses' => 'AuthController@getLogin', 'as' => 'auth.login']);
    Route::post('login', ['uses' => 'AuthController@postLogin', 'as' => 'auth.login']);

    Route::get('register', ['uses' => 'AuthController@getRegister', 'as' => 'auth.register']);
    Route::post('register', ['uses' => 'AuthController@postRegister', 'as' => 'auth.register']);
});

Route::group(['prefix' => 'users', 'middleware' => 'auth'], function () {
    Route::get('all', ['uses' => 'UserController@all', 'as' => 'user.all']);
    Route::post('search', ['uses' => 'UserController@search', 'as' => 'user.search']);

    Route::get('edit', ['uses' => 'UserController@getEditUser', 'as' => 'user.edit']);
    Route::post('edit', ['uses' => 'UserController@postEditUser', 'as' => 'user.edit']);

    Route::get('view/{id}/{slug}', ['uses' => 'UserController@viewUser', 'as' => 'user.view']);
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('add', ['uses' => 'AdminController@add', 'as' => 'admin.index']);

    Route::get('delete/{id}', ['uses' => 'AdminController@getDelete', 'as' => 'admin.delete']);
});
